<?php
namespace di\proxy;

require_once __DIR__ . '/../vendor/autoload.php';

$config = include __DIR__ . '/../config/settings.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Cookie\CookieJar;
use mef\Http\FileStream;
use mef\Http\Request;
use mef\Http\ServerRequest;
use mef\Http\Uri;
use RuntimeException;

$serverRequest = ServerRequest::fromGlobals(
	new FileStream(fopen('php://input', 'r')),
	$_SERVER,
	$_GET,
	$_POST,
	$_COOKIE,
	$_FILES
);

$params = $serverRequest->getServerParams();

if (isset($params['PATH_INFO']) === false)
{
	throw new RuntimeException('No PATH_INFO was set.');
}

$uri = new Uri(
	$config['request']['uri']['scheme'],
	'', // user
	'', // password
	$config['request']['uri']['hostname'],
	$config['request']['uri']['port'],
	$config['request']['uri']['path'] . $params['PATH_INFO'],
	$serverRequest->getUri()->getQuery()
);

$headers = [
	'host' => $config['request']['uri']['hostname'],
];

foreach ($config['request']['headers'] as $key => $value)
{
	if (is_string($value) === true)
	{
		$headers[$key] = $value;
	}
	else if ($value === true)
	{
		if ($serverRequest->hasHeader($key) === true)
		{
			$headers[$key] = $serverRequest->getHeader($key);
		}
	}
}

$request = new Request([
	'uri' => $uri,
	'method' => $serverRequest->getMethod(),
	'body' => (string) $serverRequest->getBody(),
	'headers' => $headers
]);

$incomingCookies = array_filter($config['request']['cookies'], function($cookie) {
	return $cookie !== false && $cookie !== null;
});

$cookies = [];
foreach ($incomingCookies as $incomingCookieName => $incomingCookie)
{
	if ($incomingCookie !== true)
	{
		$mappedIncomingCookieName = $incomingCookie['name'];
	}
	else
	{
		$mappedIncomingCookieName = $incomingCookieName;
	}

	if (isset($_COOKIE[$incomingCookieName]) === true)
	{
		$cookies[$mappedIncomingCookieName] = $_COOKIE[$incomingCookieName];
	}
}

$cookieJar = CookieJar::fromArray(
	$cookies,
	$config['request']['uri']['hostname']
);

$guzzle = new Client();
try
{
	$response = $guzzle->send($request, ['cookies' => $cookieJar]);
}
catch (ClientException $e)
{
	$response = $e->getResponse();
}
catch (ServerException $e)
{
	$response = $e->getResponse();
}

http_response_code($response->getStatusCode());

foreach ($config['response']['headers'] as $key => $value)
{
	if ($response->hasHeader($key) === true)
	{
		foreach ($response->getHeader($key) as $i => $value)
		{
			header("$key: $value", $i === 0);
		}
	}
}

$allowedCookies = array_filter($config['response']['cookies'], function($cookie) {
	return $cookie !== false && $cookie !== null;
});

foreach ($cookieJar->toArray() as $cookie)
{
	if (isset($allowedCookies[$cookie['Name']]) === true)
	{
		setcookie(
			$cookie['Name'],
			$cookie['Value'],
			$cookie['Expires'],
			'/',
			'', /* domain */
			$cookie['Secure'],
			$cookie['HttpOnly']
		);
	}
}

echo $response->getBody()->getContents();
