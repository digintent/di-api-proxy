<?php
return [
	'request' => [
		'uri' => [
			'scheme' => 'https',
			'hostname' => 'di-rkdev.azurewebsites.net',
			'path' => '/api',
			'port' => 443,
		],
		'headers' => [
			'Content-Type' => true,
			'Accept' => true,
			'Accept-Encoding' => true,
			'Accept-Language' => true,
			'User-Agent' => true,
		],
	],

	'response' => [
		'headers' => [
			'Content-Type' => true,
		],
	],

	'cookies' => [
		'.ASPXAUTH' => true,
	],
];